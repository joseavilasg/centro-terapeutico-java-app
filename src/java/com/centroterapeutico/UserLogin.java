/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.centroterapeutico;



import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;

//@Named(value = "userLogin")
@ManagedBean(name = "userLogin")
@RequestScoped
public class UserLogin {
    private String message ="Enter username and password.";
    private String username;
    private String password;
    public String login(){
    	if("joseavila".equalsIgnoreCase(username) && "12345".equalsIgnoreCase(password)) {
    		message =username;
    		return "ubicanos";
    	} else {
    		message ="Wrong credentials.";
    		return "index";
    	}
    }
    public String getMessage() {
	return message;
    }
    public void setMessage(String message) {
	this.message = message;
    }
    public String getUsername() {
	return username;
    }
    public void setUsername(String username) {
	this.username = username;
    }
    public String getPassword() {
	return password;
    }
    public void setPassword(String password) {
	this.password = password;
    }
} 
