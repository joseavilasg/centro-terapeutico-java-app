/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.centroterapeutico;

import java.io.Serializable;
import javax.annotation.ManagedBean;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.inject.Inject;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class LoginBean implements Serializable {

    @Inject
    private User user;
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public String enviar() {
        if (this.user.getEmail().equals("jose@gmail.com") && this.user.getPassword().equals("12345")) {
            return "success";
        } else {
            return "fail";
        }
    }
}
