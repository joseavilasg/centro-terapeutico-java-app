drop database if exists centroterapeutico;
create database centroterapeutico;
use centroterapeutico;

create table Users (
    Id INT PRIMARY KEY AUTO_INCREMENT,
    Names VARCHAR(20),
    Lastnames VARCHAR(20),
    Email VARCHAR(20),
    Password VARCHAR(20)
)ENGINE=INNODB;